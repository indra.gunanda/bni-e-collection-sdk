<?php


namespace SystemFive;


use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use SystemFive\Core\BNIEnc;
use SystemFive\Requests\BNIRequest;

class BNIEcollection
{
    protected Client $client;
    protected string $api;
    private BNIRequest $payload;
    private string $secret_key;

    /**
     * BNIEcollection constructor.
     * @param string $secret_key
     * @param false $production_status
     */
    public function __construct(string $secret_key,bool $production_status = false)
    {
        if ($production_status){
            $this->api = 'https://api.bni-ecollection.com/';
        }else{
            $this->api = 'https://apibeta.bni-ecollection.com/';
        }

        $this->secret_key = $secret_key;

        $this->client = new Client(["base_uri"=>$this->api]);
    }

    /**
     * @param $data
     * @param $type
     * @param $billing_type
     * @return BNIEcollection
     * @throws Exception
     */
    public function request_data($data, $type, $billing_type): BNIEcollection
    {
        $this->payload = new BNIRequest($data, $type, $billing_type);
        return $this;
    }

    /**
     * @return mixed
     * @throws GuzzleException
     */
    public function getResponse()
    {
        $headers = [
            "User-Agent"=>'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36'
        ];
        $encryptPayload = BNIEnc::encrypt($this->payload->toArray(),$this->payload->client_id,$this->secret_key);
        $response = $this->client->post("/",[
            RequestOptions::JSON => [
                "client_id"=>$this->payload->client_id,
                "data"=>$encryptPayload
            ],
            RequestOptions::HEADERS => $headers
        ]);
        $responseRaw = json_decode($response->getBody()->getContents(),true);
        $responseData = $responseRaw;
        if ($responseRaw["status"] === '000'){
            $responseData = BNIEnc::decrypt($responseRaw["data"],$this->payload->client_id,$this->secret_key);
        }
        return $responseData;
    }

}