<?php


namespace SystemFive\Core;

/**
 * Class BNIEnc
 * @package SystemFive\Core
 */

class BNIEnc
{

    const TIME_DIFF_LIMIT = 480;

    /**
     * @param array $json_data
     * @param $cid
     * @param $secret
     * @return string
     */

    public static function encrypt(array $json_data, $cid, $secret) {
        return self::doubleEncrypt(strrev(time()) . '.' . json_encode($json_data), $cid, $secret);
    }

    /**
     * @param $hased_string
     * @param $cid
     * @param $secret
     * @return mixed|null
     */
    public static function decrypt($hased_string, $cid, $secret) {
        $parsed_string = self::doubleDecrypt($hased_string, $cid, $secret);
        list($timestamp, $data) = array_pad(explode('.', $parsed_string, 2), 2, null);
        if (self::tsDiff(strrev($timestamp)) === true) {
            return json_decode($data, true);
        }
        return null;
    }

    /**
     * @param $ts
     * @return bool
     */
    private static function tsDiff($ts) {
        return abs($ts - time()) <= self::TIME_DIFF_LIMIT;
    }

    /**
     * @param $string
     * @param $cid
     * @param $secret
     * @return string
     */
    private static function doubleEncrypt($string, $cid, $secret) {
        $result = '';
        $result = self::enc($string, $cid);
        $result = self::enc($result, $secret);
        return strtr(rtrim(base64_encode($result), '='), '+/', '-_');
    }

    /**
     * @param $string
     * @param $key
     * @return string
     */
    private static function enc($string, $key) {
        $result = '';
        $strls = strlen($string);
        $strlk = strlen($key);
        for($i = 0; $i < $strls; $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % $strlk) - 1, 1);
            $char = chr((ord($char) + ord($keychar)) % 128);
            $result .= $char;
        }
        return $result;
    }

    /**
     * @param $string
     * @param $cid
     * @param $secret
     * @return string
     */
    private static function doubleDecrypt($string, $cid, $secret) {
        $result = base64_decode(strtr(str_pad($string, ceil(strlen($string) / 4) * 4, '=', STR_PAD_RIGHT), '-_', '+/'));
        $result = self::dec($result, $cid);
        $result = self::dec($result, $secret);
        return $result;
    }

    /**
     * @param $string
     * @param $key
     * @return string
     */
    private static function dec($string, $key) {
        $result = '';
        $strls = strlen($string);
        $strlk = strlen($key);
        for($i = 0; $i < $strls; $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % $strlk) - 1, 1);
            $char = chr(((ord($char) - ord($keychar)) + 256) % 128);
            $result .= $char;
        }
        return $result;
    }

}