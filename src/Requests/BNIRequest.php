<?php


namespace SystemFive\Requests;


use Exception;

class BNIRequest
{
    //CONST CREATE TRX
    const CREATE_BILLING = 'createbilling';
    const CREATE_BILLING_SMS = 'createbillingsms';
    const INQUIRY_BILLING = 'inquirybilling';
    const UPDATE_BILLING = 'updatebilling';
    //CONST BILLING TYPE
    const OPEN_PAYMENT = 'o';
    const FIXED_PAYMENT = 'c';
    const PARTIAL_PAYMENT = 'i';
    const MINIMUM_PAYMENT = 'm';
    const OPEN_MINIMUM_PAYMENT = 'n';
    const OPEN_MAXIMUM_PAYMENT = 'x';
    //PRIV VAR
    public $client_id;
    public  $trx_amount;
    public  $customer_name;
    public  $billing_type;
    public  $customer_email;
    public  $customer_phone;
    public  $virtual_account;
    public  $trx_id;
    public  $datetime_expired;
    public  $description;
    public  $type;
    //FIELD
    public $fields = [
        "client_id",
        "trx_amount",
        "customer_name",
        "billing_type",
        "customer_email",
        "customer_phone",
        "virtual_account",
        "trx_id",
        "datetime_expired",
        "description",
        "type",
    ];

    /**
     * BNIRequest constructor.
     * @param $data
     * @param $type
     * @param $billing_type
     * @throws Exception
     */
    public function __construct($data, $type,$billing_type)
    {
        foreach ($data as $index => $datum) {
            $this->{$index} = $datum;
        }
        if (!in_array($type,[self::CREATE_BILLING,self::UPDATE_BILLING,self::INQUIRY_BILLING,self::CREATE_BILLING_SMS])){
            throw new Exception('Invalid Requested Type');
        }
        $this->type = $type;
        $this->billing_type = $billing_type;
        $keys = array_keys($data);
        switch ($type){
            case self::CREATE_BILLING:
                $required = ["type","client_id","trx_id","trx_amount","billing_type","customer_name"];
                foreach ($required as $index => $item) {
                    if (!in_array($item,$keys)){
                        throw new Exception('Please fill required Parameter for type '.self::CREATE_BILLING);
                    }
                }

                if (!in_array($billing_type,[self::OPEN_PAYMENT,self::FIXED_PAYMENT,self::PARTIAL_PAYMENT,self::MINIMUM_PAYMENT,self::OPEN_MAXIMUM_PAYMENT,self::OPEN_MINIMUM_PAYMENT])){
                    throw new Exception('Billing Type Invalid');
                }
                $this->trx_amount = abs((int) $this->trx_amount);
                if ($billing_type === self::OPEN_PAYMENT){
                    $this->datetime_expired = '';
                    $this->trx_amount = 0;
                }

                break;
            case self::CREATE_BILLING_SMS:
                $required = ["type","client_id","trx_id","trx_amount","billing_type","customer_name"];
                foreach ($required as $index => $item) {
                    if (!in_array($item,$keys)){
                        throw new Exception('Please fill required Parameter for type '.self::CREATE_BILLING_SMS);
                    }
                }
                if (!in_array($billing_type,[self::OPEN_PAYMENT,self::FIXED_PAYMENT,self::PARTIAL_PAYMENT,self::MINIMUM_PAYMENT,self::OPEN_MAXIMUM_PAYMENT,self::OPEN_MINIMUM_PAYMENT])){
                    throw new Exception('Billing Type Invalid');
                }
                $this->trx_amount = abs((int) $this->trx_amount);
                if ($billing_type === self::OPEN_PAYMENT){
                    $this->datetime_expired = '';
                    $this->trx_amount = 0;
                }
                break;
            case self::UPDATE_BILLING:

                $required = ["type","client_id","trx_id","trx_amount","customer_name"];
                foreach ($required as $index => $item) {
                    if (!in_array($item, $keys)) {
                        throw new Exception('Please fill required Parameter for type '.self::UPDATE_BILLING);
                    }
                }
                $this->trx_amount = abs((int) $this->trx_amount);
                break;
            case self::INQUIRY_BILLING:
                $required = ["type","client_id","trx_id"];
                foreach ($required as $index => $item) {
                    if (!in_array($item, $keys)) {
                        throw new Exception('Please fill required Parameter for type '.self::INQUIRY_BILLING);
                    }
                }
                break;
            default:
                throw new Exception('Request Rejected');
        }

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $res = [];
        foreach ($this->fields as $index => $field) {
            $res[$field] = $this->{$field};
        }
        return $res;
    }
}