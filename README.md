### README
An extension to get and request BNI eCollection, this library cover function bellow 
* Create VA 
* Update VA 
* Inquiry VA 
* Callback VA 

### Usage 
basic usage create VA / Invoice 
```php
require 'vendor/autoload.php';

use SystemFive\BNIEcollection;

$request = new BNIEcollection("YOUR_SECRET_KEY");
$data_asli = [
    'client_id' => "YOUR_CID",
    'trx_id' => mt_rand(), // fill with Billing ID
    'trx_amount' => 10000,
    'billing_type' => 'c',
    'datetime_expired' => date('c', time() + 2 * 3600), // billing will be expired in 2 hours
    'virtual_account' => '',
    'type' => \SystemFive\Requests\BNIRequest::CREATE_BILLING,
    'customer_name' => 'Name',
    'customer_email' => 'indra.ganteng@gmail.com',
    'customer_phone' => '0812142676xx',
];

try {
    $request->request_data($data_asli, \SystemFive\Requests\BNIRequest::CREATE_BILLING, \SystemFive\Requests\BNIRequest::FIXED_PAYMENT);
    $response = $request->getResponse();
    var_dump($response);
} catch (Exception $e) {
    var_dump($e->getMessage());
} catch (\GuzzleHttp\Exception\GuzzleException $e) {
    var_dump($e->getMessage());
}
```

there are have 4 type request 
* createbilling
* createbillingsms
* inquirybilling
* updatebilling

for response you can see docs of BNI E-Collection API . 
let contact BNI CS <br>

Usage for Retrieve Callback

```php

$callbackFromBNI = [
    "client_id"=>"YOUR_CID_FROM_CALLBACK",
    "data"=>"YOUR_HASH_DATA"
];

\SystemFive\Core\BNIEnc::decrypt($callbackFromBNI["data"],$callbackFromBNI["client_id"],"YOUR_SECRET");
```

### Yup It's Done 

You can help me buy a coffee with PAYPAL on **indra.gunanda@gmail.com** <br>

VvV if you Indonesian you can use this VvV

![image](https://ipfs.io/ipfs/QmeFCFbrTYQHDwrssbp8toSPUvVPND59PytM8f7wMTN4ui)
